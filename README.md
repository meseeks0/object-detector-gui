# Object Detector GUI #

A Qt widget that runs YOLO object detection on single images or batches of images in a directory. 
Object detection is performed using Darknet from https://github.com/AlexeyAB/darknet and originally developed at https://github.com/pjreddie/darknet.