#include "detectionmain.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setStyle("fusion");
    DetectionMain w;
    w.show();
    return a.exec();
}
