#include "detectionmain.h"
#include "ui_detectionmain.h"
#include "detectorworker.h"

#include <QFileDialog>
#include <QDir>
#include <QLabel>
#include <QDebug>
#include <QMetaType>
#include <opencv2/opencv.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <yolo_v2_class.hpp>

DetectionMain::DetectionMain(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::DetectionMain)
{
    qRegisterMetaType<std::string>();
    qRegisterMetaType<std::vector<bbox_t>>();
    qRegisterMetaType<QFileInfoList>();
//    qRegisterMetaType<vector_preds>();
    ui->setupUi(this);
    labelImage = new QLabel;
    labelImage->setBackgroundRole(QPalette::Base);
    labelImage->setSizePolicy(QSizePolicy::Ignored, QSizePolicy::Ignored);
    labelImage->setScaledContents(true);
    ui->scrollArea->setBackgroundRole(QPalette::Dark);
    ui->scrollArea->setVisible(true);
    ui->scrollArea->setWidget(labelImage);
    setup();
}

DetectionMain::~DetectionMain()
{
    detectorThread.requestInterruption();
    detectorThread.quit();
    while (!detectorThread.isFinished());

//    delete detectorThread;
    delete ui;
}

void DetectionMain::closeEvent(QCloseEvent *event)
{
    emit sendMainWindowClose();
}

void DetectionMain::setup()
{
//    _detectorThread = new QThread();
    DetectorWorker *detectorWorker = new DetectorWorker();

    connect(this, SIGNAL(sendImage(QImage)), this, SLOT(receiveImage(QImage)));
    connect(this, SIGNAL(sendImageName(QString)), this, SLOT(receiveImageName(QString)));
    connect(this, SIGNAL(sendModelConfigPath(QString)), this, SLOT(receiveModelConfigPath(QString)));
    connect(this, SIGNAL(sendModelWeightsPath(QString)), this, SLOT(receiveModelWeightsPath(QString)));
    connect(this, SIGNAL(sendModelObjectNames(QString)), this, SLOT(receiveModelObjectNames(QString)));

    connect(this, SIGNAL(sendSetup(std::string, std::string)), detectorWorker, SLOT(receiveSetup(std::string, std::string)));
    connect(detectorWorker, SIGNAL(sendSetupComplete(QString)), this, SLOT(receiveSetupComplete(QString)));
    connect(this, SIGNAL(sendPredictImage(QString)), detectorWorker, SLOT(receivePredictImage(QString)));
    connect(detectorWorker, SIGNAL(sendPredictions(std::vector<bbox_t>)), this, SLOT(receivePredictions(std::vector<bbox_t>)));
    connect(this, SIGNAL(sendPredictImageBatch(QFileInfoList)), detectorWorker, SLOT(receivePredictImageBatch(QFileInfoList)));
    connect(detectorWorker, SIGNAL(sendCurrentImage(QString)), this, SLOT(receiveCurrentImage(QString)));
    connect(&detectorThread, SIGNAL(finished()), detectorWorker, SLOT(deleteLater()));
    connect(this, SIGNAL(sendMainWindowClose()), detectorWorker, SLOT(receiveMainWindowClose()));

    detectorWorker->moveToThread(&detectorThread);
    detectorThread.start();

    ui->texteditConfig->setText(_modelConfigPath);
    ui->texteditWeights->setText(_modelWeightsPath);
    ui->texteditNames->setText(_modelObjectNames);
    ui->pbDetectImage->setEnabled(modelLoaded);
    ui->pbDetectImageBatch->setEnabled(modelLoaded);

    QPalette pal = ui->pbInitialize->palette();
    pal.setColor(QPalette::Button, QColor(Qt::red));
    ui->pbInitialize->setAutoFillBackground(true);
    ui->pbInitialize->setPalette(pal);
    ui->pbInitialize->update();
}

QImage DetectionMain::Mat2QImage(cv::Mat const& src)
{
//     cv::Mat temp; // make the same cv::Mat
//     cvtColor(src, temp, cv::COLOR_BGR2RGB); // cvtColor Makes a copy, that what i need
//     QImage dest((const uchar *) temp.data, temp.cols, temp.rows, temp.step, QImage::Format_RGB888);
//     dest.bits(); // enforce deep copy, see documentation
//     // of QImage::QImage ( const uchar * data, int width, int height, Format format )
//     return dest;

    QImage qImg = QImage((uchar*)src.data, src.cols, src.rows, src.step, QImage::Format_RGB888);
    return qImg;
}

cv::Mat DetectionMain::QImage2Mat(QImage const& src)
{
     cv::Mat tmp(src.height(), src.width(), CV_8UC3, (uchar*)src.bits(), src.bytesPerLine());
     cv::Mat result; // deep copy just in case (my lack of knowledge with open cv)
     cvtColor(tmp, result, cv::COLOR_BGR2RGB);
     return result;
}

void DetectionMain::readImage(QString fileName)
{
    _imagePath = fileName;
    _imageOriginal = cv::imread(_imagePath.toUtf8().constData(), cv::IMREAD_COLOR);
}

void DetectionMain::readFolder(QString path)
{
    _folderPath = path;
    QDir imageFolder(_folderPath);
    _imageBatchList = imageFolder.entryInfoList(QStringList() << "*.BMP" << "*.bmp", QDir::Files, QDir::Time);
    _imagePath = _imageBatchList.first().filePath();
}

void DetectionMain::updateDisplayImage()
{
    QStringList splitName = _imagePath.split("/");
    _imageName = splitName.at(splitName.size() - 1);
    emit sendImage(_imageOutput);
    emit sendImageName(_imageName);
}

void DetectionMain::predictImage(Detector &detector)
{
    auto img = detector.load_image(_imagePath.toUtf8().constData());
    std::shared_ptr<image_t> darknetImage;
    std::vector<bbox_t> result_vec = detector.detect(img, 0.5);

    cv::Scalar color = cv::Scalar(255, 0, 0);
    for (bbox_t bbox : result_vec)
    {
        cv::rectangle(_imageOriginal, cv::Point(bbox.x, bbox.y), cv::Point(bbox.x + bbox.w, bbox.y + bbox.h), color, 3);
    }
    _imageOutput = Mat2QImage(_imageOriginal);
}

void DetectionMain::processImageBatch()
{
    for (QFileInfo imageItem : _imageBatchList)
    {
        _imagePath = imageItem.filePath();
        emit sendPredictImage(_imagePath);
        updateDisplayImage();
    }
}

void DetectionMain::writePredsToCSV(QString imagePath, std::vector<bbox_t> preds)
{
    QStringList splitPath = imagePath.split("/");
    QString imageFileName = splitPath.at(splitPath.size() - 1);
    QStringList splitName = imageFileName.split(".");
    QString extension = splitName.at(splitName.size() - 1);
    QString csvFileName = imagePath.left(imagePath.size() - extension.size()).append("csv");
    QFile csv(csvFileName);
    if (csv.open(QFile::WriteOnly|QFile::Truncate|QIODevice::Append) && !preds.empty())
    {
        QTextStream stream(&csv);
        stream << "object_id,probability,x,y,w,h\n";
        for (bbox_t bbox : preds)
        {
            stream << bbox.obj_id << "," << bbox.prob << "," << bbox.x << "," << bbox.y << "," << bbox.w << "," << bbox.h << "\n";
        }
    }
    csv.close();
}

void DetectionMain::receiveImage(QImage image)
{
    labelImage->setPixmap(QPixmap::fromImage(image));
    ui->scrollArea->setWidgetResizable(true);
}

void DetectionMain::receiveImageName(QString fileName)
{
    ui->labelImageName->setText(fileName);
}

void DetectionMain::receiveModelConfigPath(QString fileName)
{
    ui->texteditConfig->setText(fileName);
}

void DetectionMain::receiveModelWeightsPath(QString fileName)
{
    ui->texteditWeights->setText(fileName);
}

void DetectionMain::receiveModelObjectNames(QString fileName)
{
    ui->texteditNames->setText(fileName);
}

void DetectionMain::receiveSetupComplete(QString message)
{
    qDebug() << message;
    modelLoaded = true;
    ui->pbDetectImage->setEnabled(modelLoaded);
    ui->pbDetectImageBatch->setEnabled(modelLoaded);

    QPalette pal = ui->pbInitialize->palette();
    pal.setColor(QPalette::Button, QColor(Qt::lightGray));
    ui->pbInitialize->setAutoFillBackground(true);
    ui->pbInitialize->setPalette(pal);
    ui->pbInitialize->update();
}

void DetectionMain::receivePredictions(std::vector<bbox_t> preds)
{
    cv::Scalar color = cv::Scalar(255, 0, 0);
    if (!preds.empty())
    {
        for (bbox_t bbox : preds)
        {
            cv::rectangle(_imageOriginal, cv::Point(bbox.x, bbox.y), cv::Point(bbox.x + bbox.w, bbox.y + bbox.h), color, 3);
        }
    }
    writePredsToCSV(_imagePath, preds);
    _imageOutput = Mat2QImage(_imageOriginal);
    updateDisplayImage();
}

void DetectionMain::receiveCurrentImage(QString imagePath)
{
    readImage(imagePath);
    _imageOutput = Mat2QImage(_imageOriginal);
    updateDisplayImage();
}

void DetectionMain::on_pbSelectImage_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Image", QDir::homePath());
    if (!fileName.isEmpty())
    {
        readImage(fileName);
    //    QImage output((const unsigned char *)_imagePredictions.data, _imagePredictions.cols, _imagePredictions.rows, _imagePredictions.step, QImage::Format_RGB888);
        _imageOutput = Mat2QImage(_imageOriginal);
        updateDisplayImage();
    }
}

void DetectionMain::on_pbSelectFolder_clicked()
{
    QString pathName = QFileDialog::getExistingDirectory(this, "Select Image Folder", QDir::homePath(), QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    if (!pathName.isEmpty())
    {
        readFolder(pathName);
        readImage(_imagePath);
        _imageOutput = Mat2QImage(_imageOriginal);
        updateDisplayImage();
    }
}

void DetectionMain::on_pbDetectImage_clicked()
{
    emit sendPredictImage(_imagePath);
}

void DetectionMain::on_pbDetectImageBatch_clicked()
{
    emit sendPredictImageBatch(_imageBatchList);
}

void DetectionMain::on_pbModelConfig_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Config File", QDir::homePath());
    _modelConfigPath = fileName;
    emit sendModelConfigPath(_modelConfigPath);
}

void DetectionMain::on_pbModelWeights_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Weights File", QDir::homePath());
    _modelWeightsPath = fileName;
    emit sendModelWeightsPath(_modelWeightsPath);
}

void DetectionMain::on_pbModelNames_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, "Select Object Names File", QDir::homePath());
    _modelObjectNames = fileName;
    emit sendModelObjectNames(_modelObjectNames);
}

void DetectionMain::on_pbInitialize_clicked()
{
    modelLoaded = false;
    ui->pbDetectImage->setEnabled(modelLoaded);
    ui->pbDetectImageBatch->setEnabled(modelLoaded);

    std::string  names_file = _modelObjectNames.toUtf8().constData();
    std::string  cfg_file = _modelConfigPath.toUtf8().constData();
    std::string  weights_file = _modelWeightsPath.toUtf8().constData();
    emit sendSetup(cfg_file, weights_file);
}
