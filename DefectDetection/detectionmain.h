#ifndef DETECTIONMAIN_H
#define DETECTIONMAIN_H

#include <QMainWindow>
#include <QThread>
#include <QDir>
#include <QLabel>
#include "detectorworker.h"
#include <opencv2/core/core.hpp>
#include <yolo_v2_class.hpp>

QT_BEGIN_NAMESPACE
namespace Ui { class DetectionMain; }
QT_END_NAMESPACE

Q_DECLARE_METATYPE(std::string);
Q_DECLARE_METATYPE(std::vector<bbox_t>);
Q_DECLARE_METATYPE(QFileInfoList);

class DetectionMain : public QMainWindow
{
    Q_OBJECT
    QThread detectorThread;

private:
    Ui::DetectionMain *ui;
    QLabel *labelImage;
    cv::Mat _imageOriginal;
    cv::Mat _imagePredictions;
    QImage _imageOutput;
//    QThread *_detectorThread;
    DetectorWorker *_detectorWorker;
    QFileInfoList _imageBatchList;
    QString _imagePath;
    QString _folderPath;
    QString _imageName;
    QString _modelConfigPath{"C:\\Users\\dd7291\\Documents\\Qt\\defects\\yolov3-tiny-defects.cfg"};
    QString _modelWeightsPath{"C:\\Users\\dd7291\\Documents\\Qt\\defects\\yolov3-tiny-defects_final-20191220-2145.weights"};
    QString _modelObjectNames{"C:\\Users\\dd7291\\Documents\\Qt\\defects\\classes.names"};
    bool modelLoaded{false};

    void closeEvent(QCloseEvent *event);

public:
    QImage Mat2QImage(cv::Mat const& src);
    cv::Mat QImage2Mat(QImage const& src);

    DetectionMain(QWidget *parent = nullptr);
    ~DetectionMain();
    void setup();
    void readImage(QString fileName);
    void readFolder(QString path);
//    void processImage();
    void processImageBatch();
    void updateDisplayImage();
    void predictImage(Detector &detector);
    void selectImageFromBatch();
    void writePredsToCSV(QString imagePath, std::vector<bbox_t> preds);

private slots:
    void receiveImage(QImage image);
    void receiveImageName(QString fileName);
    void receiveModelConfigPath(QString fileName);
    void receiveModelWeightsPath(QString fileName);
    void receiveModelObjectNames(QString fileName);
    void receiveSetupComplete(QString message);
    void receivePredictions(std::vector<bbox_t> preds);
    void receiveCurrentImage(QString imagePath);
    void on_pbSelectImage_clicked();
    void on_pbSelectFolder_clicked();
    void on_pbDetectImage_clicked();
    void on_pbModelConfig_clicked();
    void on_pbModelWeights_clicked();
    void on_pbModelNames_clicked();

    void on_pbInitialize_clicked();

    void on_pbDetectImageBatch_clicked();

signals:
    void sendSetup(std::string cfg_file, std::string weights_file);
    void sendImage(QImage image);
    void sendPredictImage(QString imagePath);
    void sendPredictImageBatch(QFileInfoList imageBatchList);
    void sendImageName(QString fileName);
    void sendModelConfigPath(QString fileName);
    void sendModelWeightsPath(QString fileName);
    void sendModelObjectNames(QString fileName);
    void sendMainWindowClose();
};
#endif // DETECTIONMAIN_H
