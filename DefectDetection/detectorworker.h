#ifndef DETECTORWORKER_H
#define DETECTORWORKER_H

#include <QObject>
#include <QDir>
#include <QDebug>
#include <QThread>
#include <yolo_v2_class.hpp>

//typedef std::vector<std::vector<int>> vector_preds;
//Q_DECLARE_METATYPE(std::vector<bbox_t>);

class DetectorWorker : public QObject
{
    Q_OBJECT

private:
    bool mainWindowClosed{false};

public:
    Detector *detector;
    std::string namesFile{};

    explicit DetectorWorker(QObject *parent = nullptr);
    ~DetectorWorker();

public slots:
    void receiveSetup(std::string configFile, std::string weightsFile);
    void receivePredictImage(QString imagePath);
    void receivePredictImageBatch(QFileInfoList imageBatchList);
    void receiveMainWindowClose();

signals:
    void sendSetupComplete(QString message);
    void sendPredictions(std::vector<bbox_t>);
    void sendCurrentImage(QString imagePath);
};

#endif // DETECTORWORKER_H
