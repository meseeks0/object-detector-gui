#include "detectorworker.h"
#include <opencv2/opencv.hpp>
#include <yolo_v2_class.hpp>

DetectorWorker::DetectorWorker(QObject *parent) :
    QObject(parent),
    detector{nullptr},
    namesFile{""}
{
//    qRegisterMetaType<std::vector<bbox_t>>();
}

DetectorWorker::~DetectorWorker()
{
    delete detector;
}

void DetectorWorker::receiveMainWindowClose()
{
    mainWindowClosed = true;
}

void DetectorWorker::receiveSetup(std::string configFile, std::string weightsFile)
{
    detector = new Detector(configFile, weightsFile);
    QString message = "Initialize Complete";
    emit sendSetupComplete(message);
}

void DetectorWorker::receivePredictImage(QString imagePath)
{
    auto img = detector->load_image(imagePath.toUtf8().constData());
    std::vector<bbox_t> preds = detector->detect(img, 0.5);
    emit sendPredictions(preds);
}

void DetectorWorker::receivePredictImageBatch(QFileInfoList imageBatchList)
{
    for (QFileInfo imageItem : imageBatchList)
    {
        QString imagePath = imageItem.filePath();
        qDebug() << imagePath;
        auto img = detector->load_image(imagePath.toUtf8().constData());
        std::vector<bbox_t> preds = detector->detect(img, 0.5);
        QThread::sleep(2);
        emit sendCurrentImage(imagePath);
        emit sendPredictions(preds);
        Detector::free_image(img);

        if (QThread::currentThread()->isInterruptionRequested()) return;
    }
}

